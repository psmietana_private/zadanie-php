<?php

namespace App\Http\Controllers;

use App\Models\ShoppingCart;
use App\Models\ShoppingCartPosition;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::all();

        $cart = (null !== $cartId = $request->session()->get('cart_id'))
            ? ShoppingCart::where('user_id', Auth::user()->getAuthIdentifier())
                ->andWhere('id', $cartId)
                ->firstOrFail()
            : null;
        $cartPositions = $cart
            ? ShoppingCartPosition::where('shopping_cart_id')->get()
            : [];

        return view('products', compact('products', 'cartPositions'));
    }

    public function addProductToCart(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $cart = (null !== $cartId = $request->session()->get('cart_id'))
                ? ShoppingCart::where('user_id', $user->getAuthIdentifier())->andWhere('id', $cartId)->firstOrFail()
                : ShoppingCart::firstOrCreate(['user_id' => $user->getAuthIdentifier()]);

            $productId = $request->get('product_id');
            $position = ShoppingCartPosition::where('product_id', $productId)
                ->andWhere('shopping_cart_id', $cart->id)
                ->firstOrCreate();
            $position->increaseQuantityBy(1);
            $position->save();

            DB::commit();
            $request->session()->put('cart_id', $cart->id);

            $request->session()->flash('success', 'Dodano produkt do koszyka');
        } catch (Throwable $exception)   {
            DB::rollBack();

            $request->session()->flash('danger', 'Błąd podczas dodawania do koszyka');
        }

        return redirect()->route('dashboard');
    }
}
