<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

class ShoppingCartPosition extends Model
{
    public function product()
    {
        return $this->hasOne(Product::class);
    }

    public function increaseQuantityBy(integer $addedQuantity = 1): void
    {
        if ($addedQuantity < 0) {
            throw new InvalidArgumentException('Cannot add negative quantity to shopping cart position');
        }

        $this->quantity += $addedQuantity;
    }
}
