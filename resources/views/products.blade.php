<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Product list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-2xl mx-auto sm:px-2 lg:px-2">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2>Product list:</h2>
                    <table class="table-auto">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td class="pr-4">
                                        {{ $product->name }}
                                    </td>
                                    <td>
                                        <form method="post" action="{{ route('add-to-cart') }}">
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <input type="hidden" name="_token" :value="csrf">
                                            <x-button>
                                                To cart
                                            </x-button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>

                    <h2>{{ __('Cart content') }}</h2>

                    <table class="table-auto">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($cartPositions as $cartPosition)
                                <tr>
                                    <td class="pr-4">
                                        <a href="#">{{ $cartPosition->product()->name }}</a>
                                    </td>
                                    <td>
                                        {{ $cartPosition->quantity }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
